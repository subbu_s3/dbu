﻿Public Class GlobalVariable

    ' public get, and private set for strict access control
    Private Shared bbackup_conn As String
    Private Shared sSettingSelected As String

    Public Shared Property Backup_conn() As String
        Get
            Return bbackup_conn
        End Get
        Set(ByVal value As String)
            bbackup_conn = value
        End Set
    End Property


    Public Shared Property SettingSelected() As String
        Get
            Return sSettingSelected
        End Get
        Set(ByVal value As String)
            sSettingSelected = value
        End Set
    End Property

    ' GlobalInt can be changed only via this method
    Public Shared Sub SetGlobalInt(ByVal Backup_conn_str As String)
        Backup_conn = Backup_conn_str
    End Sub
End Class
