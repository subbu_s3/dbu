﻿Imports System
Imports System.Data
Imports System.Linq
Imports System.Text
Imports System.IO
Imports System.Collections
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Data.OleDb
Imports System.Threading.Tasks
Imports System.Windows.Forms


Namespace NSDbUtility

    Class DbUtility

        Private DbConn As OleDbConnection = New OleDbConnection

        Private DbCommand As OleDbCommand = New OleDbCommand

        Private DbAdapter As OleDbDataAdapter = New OleDbDataAdapter

        Private DbTran As OleDbTransaction

        Private dr As OleDbDataReader

        Private strConnString As String = ""

        'private string strConnString = ConfigurationManager.ConnectionStrings["Connectionstring"].ConnectionString;
        'private string strConnString = @"Data Source=66.135.55.178;Initial Catalog=spectrum_Lonavaladiary;User ID=lonavaladiary;Password=9762927302";
        'private string strConnString = @"Data Source=.\OleDbEXPRESS;AttachDbFilename=E:\BACKUP_6-9-2013\DB\MIS.mdf;Integrated Security=True;Connect Timeout=30;User Instance=True";
        'private string strConnString = @"Data Source=Viren-Pc;Initial Catalog=SAMPARC_DB;Integrated Security=True";
        'private string strConnString = @"Data Source=208.91.198.174;Initial Catalog=litscmza_SAMPARCMIS;User ID=virenvish;Password=5@mp@rc#2013";
        Public Sub setConnString(ByVal strConn As String)
            Try
                Me.strConnString = strConn
            Catch exp As Exception
                Throw exp
            End Try

        End Sub

        Public Function RETRIEVECONNECTION() As String
            Dim connectionStr As String = ""
            Try
                If (GlobalVariable.Backup_conn Is Nothing) Then
                    Dim file As String = (Application.StartupPath + "\DB\SQLCONNECT.txt")
                    If (System.IO.File.Exists(file) = True) Then
                        Dim objReader As System.IO.StreamReader = New System.IO.StreamReader(file)

                        While (objReader.Peek <> -1)
                            connectionStr = (connectionStr + objReader.ReadLine)
                            ' & vbNewLine

                        End While

                        objReader.Close()
                        'License_Decy_Text = Decrypt(License_Ency_Text);
                        'validMId = License_Decy_Text;
                        'if ((validMId == null | ValidateMacId() == false))
                        '{
                        '    MessageBox.Show("Invalid installation. Contact admin, program will now close.");
                        '    System.Environment.Exit(0);
                        '}
                    Else
                        connectionStr = ("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" _
                                    + (Application.StartupPath + "\DB\Osworld.mdb;Jet OLEDB:Database Password=blackpearl;"))
                        'MessageBox.Show("License File Does Not Exist. Contact admin, program will now close.");
                        'System.Environment.Exit(0);
                    End If

                Else
                    connectionStr = ("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" _
                                + (GlobalVariable.Backup_conn + ";Jet OLEDB:Database Password=blackpearl;"))
                End If
                GlobalVariable.SettingSelected = connectionStr
            Catch ex As System.Exception

            End Try

            Return connectionStr
        End Function

        Public Function getConnString() As String
            Try
                Me.strConnString = Me.RETRIEVECONNECTION
                Return Me.strConnString
            Catch exp As Exception
                Throw exp
            End Try

        End Function

        Public Sub createConn()
            Try
                Me.strConnString = Me.RETRIEVECONNECTION
                Me.DbConn.ConnectionString = Me.strConnString
                Me.DbConn.Open()
            Catch exp As Exception
                Throw exp
            End Try

        End Sub

        Public Sub closeConnection()
            Try
                If (Me.DbConn.State <> 0) Then
                    Me.DbConn.Close()
                End If

            Catch exp As Exception
                Throw exp
            End Try

        End Sub

        Public Sub beginTrans()
            Try
                If (Me.DbTran Is Nothing) Then
                    If (Me.DbConn.State = 0) Then
                        Me.createConn()
                    End If

                    Me.DbTran = Me.DbConn.BeginTransaction
                    Me.DbCommand.Transaction = Me.DbTran
                    'DbAdapter.SelectCommand.Transaction = DbTran;
                    'DbAdapter.InsertCommand.Transaction = DbTran;
                    'DbAdapter.UpdateCommand.Transaction = DbTran;
                    'DbAdapter.DeleteCommand.Transaction = DbTran;
                    'DbTran.Save("Current");
                End If

            Catch exp As Exception
                Throw exp
            End Try

        End Sub

        Public Sub commitTrans()
            Try
                If (Not (Me.DbTran) Is Nothing) Then
                    Me.DbTran.Commit()
                    Me.DbTran = Nothing
                End If

            Catch exp2 As Exception
                Me.DbConn.Close()
                Throw exp2
            End Try

        End Sub

        Public Sub rollbackTrans()
            Try
                If (Not (Me.DbTran) Is Nothing) Then
                    Me.DbTran.Rollback()
                End If

            Catch exp As Exception
                Throw exp
            Finally
                Me.DbTran.Dispose()
                Me.DbConn.Close()
                Me.DbAdapter.Dispose()
            End Try

        End Sub

        ''' <summary>  
        ''' Fills the Dataset dset and its Table tblname via stored procedure provided as spName arguement.Takes Parameters as param  
        ''' </summary>  
        ''' <param name="dSet"></param>  
        ''' <param name="spName"></param>  
        ''' <param name="param"></param>  
        ''' <param name="tblName"></param>  
        Public Overloads Sub selectStoredProcedure(ByVal dSet As DataSet, ByVal spName As String, ByVal param As Hashtable, ByVal tblName As String)
            Try
                If (Me.DbConn.State = 0) Then
                    Me.createConn()
                End If

                Me.DbCommand.Connection = Me.DbConn
                Me.DbCommand.CommandText = spName
                Me.DbCommand.CommandType = CommandType.StoredProcedure
                For Each para As String In param.Keys
                    Me.DbCommand.Parameters.AddWithValue(para, param(para))
                Next
                'toclear para .......Viren
                param.Clear()
                '........................
                Me.DbAdapter.Fill(dSet, tblName)
            Catch exp As Exception
                Throw exp
            End Try

        End Sub

        Public Overloads Sub selectStoredProcedure(ByVal dSet As DataSet, ByVal spName As String, ByVal tblName As String)
            Try
                If (Me.DbConn.State = 0) Then
                    Me.createConn()
                End If

                Me.DbCommand.Connection = Me.DbConn
                Me.DbCommand.CommandText = spName
                Me.DbCommand.CommandType = CommandType.StoredProcedure
                Me.DbAdapter.SelectCommand = Me.DbCommand
                Me.DbAdapter.Fill(dSet, tblName)
            Catch exp As Exception
                Throw exp
            End Try

        End Sub

        Public Sub selectQuery(ByVal dSet As DataSet, ByVal query As String, ByVal tblName As String)
            Try
                If (Me.DbConn.State = 0) Then
                    Me.createConn()
                End If

                Me.DbCommand.CommandTimeout = 600
                Me.DbCommand.Connection = Me.DbConn
                Me.DbCommand.CommandText = query
                Me.DbCommand.CommandType = CommandType.Text
                Me.DbAdapter = New OleDbDataAdapter(Me.DbCommand)
                Me.DbAdapter.Fill(dSet, tblName)
            Catch exp As Exception
                Me.DbAdapter.Dispose()
                Me.DbConn.Close()
                Throw exp
            Finally
                Me.DbAdapter.Dispose()
                Me.DbConn.Close()
            End Try

        End Sub

        Public Function executeQuery(ByVal query As String) As Integer
            Try
                If (Me.DbConn.State = 0) Then
                    Me.createConn()
                End If

                Me.DbCommand.CommandTimeout = 400
                Me.DbCommand.Connection = Me.DbConn
                Me.DbCommand.CommandText = query
                Me.DbCommand.CommandType = CommandType.Text
                Return Me.DbCommand.ExecuteNonQuery
            Catch exp As Exception
                Throw exp
            Finally
                Me.DbAdapter.Dispose()
                Me.DbConn.Close()
            End Try

        End Function

        Public Function executeStoredProcedure(ByVal spName As String, ByVal param As Hashtable) As Integer
            Try
                If (Me.DbConn.State = 0) Then
                    Me.createConn()
                End If

                Me.DbCommand.Connection = Me.DbConn
                Me.DbCommand.CommandText = spName
                Me.DbCommand.CommandType = CommandType.StoredProcedure
                Me.DbCommand.Parameters.Clear()
                For Each para As String In param.Keys
                    Me.DbCommand.Parameters.AddWithValue(para, param(para))
                Next
                Return Me.DbCommand.ExecuteNonQuery
            Catch exp As Exception
                Throw exp
            End Try

        End Function

        Public Function returnint32(ByVal strOleDb As String) As Integer
            Try
                If (Me.DbConn.State = 0) Then
                    Me.createConn()
                Else
                    Me.DbConn.Close()
                    Me.createConn()
                End If

                Me.DbCommand.Connection = Me.DbConn
                Me.DbCommand.CommandText = strOleDb
                Me.DbCommand.CommandType = CommandType.Text
                Return CType(Me.DbCommand.ExecuteScalar, Integer)
            Catch exp As Exception
                Return 0
            End Try

        End Function

        Public Function returnint64(ByVal strOleDb As String) As Int64
            Try
                If (Me.DbConn.State = 0) Then
                    Me.createConn()
                End If

                Me.DbCommand.Connection = Me.DbConn
                Me.DbCommand.CommandText = strOleDb
                Me.DbCommand.CommandType = CommandType.Text
                Return CType(Me.DbCommand.ExecuteScalar, Int64)
            Catch exp As Exception
                Throw exp
            End Try

        End Function

        Public Function executeDataSet(ByVal dSet As DataSet, ByVal tblName As String, ByVal strOleDb As String) As Integer
            Try
                If (Me.DbConn.State = 0) Then
                    Me.createConn()
                End If

                Me.DbAdapter.SelectCommand.CommandText = strOleDb
                Me.DbAdapter.SelectCommand.CommandType = CommandType.Text
                Dim DbCommandBuilder As OleDbCommandBuilder = New OleDbCommandBuilder(Me.DbAdapter)
                Return Me.DbAdapter.Update(dSet, tblName)
            Catch exp As Exception
                Throw exp
            End Try

        End Function

        Public Function checkDbConnection() As Boolean
            Dim _flag As Integer = 0
            Try
                If (Me.DbConn.State = ConnectionState.Open) Then
                    Me.DbConn.Close()
                End If

                Me.DbConn.ConnectionString = Me.getConnString
                Me.DbConn.Open()
                _flag = 1
            Catch ex As Exception
                _flag = 0
            End Try

            If (_flag = 1) Then
                Me.DbConn.Close()
                _flag = 0
                Return True
            Else
                Return False
            End If

        End Function

        Public Function GetColumnValue(ByVal Query As String) As String
            Try
                If (Me.DbConn.State = 0) Then
                    Me.createConn()
                End If

                Me.DbCommand.CommandTimeout = 120
                Me.DbCommand.Connection = Me.DbConn
                Me.DbCommand.CommandType = CommandType.Text
                Me.DbCommand.CommandText = Query
                Dim objResult As Object = Me.DbCommand.ExecuteScalar
                If (objResult Is Nothing) Then
                    Return ""
                End If

                If (objResult = System.DBNull.Value) Then
                    Return ""
                Else
                    Return Convert.ToString(objResult)
                End If

            Catch ex As Exception
                Throw ex
            Finally
                Me.DbAdapter.Dispose()
                Me.DbConn.Close()
            End Try

        End Function

        Public Function GetColumnValueObject(ByVal Query As String) As Object
            Try
                If (Me.DbConn.State = 0) Then
                    Me.createConn()
                End If

                Me.DbCommand.CommandTimeout = 120
                Me.DbCommand.Connection = Me.DbConn
                Me.DbCommand.CommandType = CommandType.Text
                Me.DbCommand.CommandText = Query
                Dim objResult As Object = Me.DbCommand.ExecuteScalar
                Return objResult
            Catch ex As Exception
                Throw ex
            Finally
                Me.DbAdapter.Dispose()
                Me.DbConn.Close()
            End Try

        End Function

        Public Function getDataSet(ByVal query As String) As DataSet
            Dim mydata As DataSet = New DataSet
            Try
                If (Me.DbConn.State = 0) Then
                    Me.createConn()
                End If

                Me.DbCommand.Connection = Me.DbConn
                Me.DbCommand.CommandText = query
                Me.DbCommand.CommandType = CommandType.Text
                Me.DbAdapter = New OleDbDataAdapter(Me.DbCommand)
                Me.DbAdapter.Fill(mydata)
                Return mydata
            Catch exp As Exception
                Throw exp
            Finally
                Me.DbAdapter.Dispose()
                Me.DbConn.Close()
            End Try

        End Function

        Public Function fillList(ByVal tableName As String, ByVal column As String) As DataSet
            Dim query As String = ("select " _
                        + (column + (" from " + tableName)))
            Dim mydata As DataSet = New DataSet
            Try
                If (Me.DbConn.State = 0) Then
                    Me.createConn()
                End If

                Me.DbCommand.Connection = Me.DbConn
                Me.DbCommand.CommandText = query
                Me.DbCommand.CommandType = CommandType.Text
                Me.DbAdapter = New OleDbDataAdapter(Me.DbCommand)
                Me.DbAdapter.Fill(mydata)
                Return mydata
            Catch exp As Exception
                Throw exp
            Finally
                Me.DbAdapter.Dispose()
                Me.DbConn.Close()
            End Try

        End Function

        Public Function getDataTable(ByVal query As String) As DataTable
            Dim mydata As DataTable = New DataTable
            Try
                If (Me.DbConn.State = 0) Then
                    Me.createConn()
                End If

                Me.DbCommand.CommandTimeout = 400
                Me.DbCommand.Connection = Me.DbConn
                Me.DbCommand.CommandText = query
                Me.DbCommand.CommandType = CommandType.Text
                Me.DbAdapter = New OleDbDataAdapter(Me.DbCommand)
                Me.DbAdapter.Fill(mydata)
                Return mydata
            Catch exp As Exception
                Throw exp
            Finally
                Me.DbAdapter.Dispose()
                Me.DbConn.Close()
            End Try

        End Function

        Public Function getDataTablefrmSP(ByVal spName As String, ByVal param As Hashtable) As DataTable
            Dim mydata As DataTable = New DataTable
            Try
                If (Me.DbConn.State = 0) Then
                    Me.createConn()
                End If

                Me.DbCommand.CommandTimeout = 400
                Me.DbCommand.Connection = Me.DbConn
                Me.DbCommand.CommandText = spName
                Me.DbCommand.CommandType = CommandType.StoredProcedure
                Me.DbCommand.Parameters.Clear()
                For Each para As String In param.Keys
                    Me.DbCommand.Parameters.AddWithValue(para, param(para))
                Next
                Me.DbAdapter = New OleDbDataAdapter(Me.DbCommand)
                Me.DbAdapter.Fill(mydata)
                Return mydata
            Catch exp As Exception
                Throw exp
            Finally
                Me.DbAdapter.Dispose()
                Me.DbConn.Close()
            End Try

        End Function

        Public Function GetReader(ByVal Qry As String) As OleDbDataReader
            Dim mydata As OleDbDataReader
            Try
                If (Me.DbConn.State = 0) Then
                    Me.createConn()
                End If

                Me.DbCommand.Connection = Me.DbConn
                Me.DbCommand.CommandText = Qry
                Me.DbCommand.CommandType = CommandType.Text
                mydata = Me.DbCommand.ExecuteReader
                Return mydata
            Catch exp As Exception
                Throw exp
            Finally
                'DbAdapter.Dispose();
                ' DbConn.Close();
            End Try

        End Function

        Public Overloads Sub selectStoredProcedure(ByVal ds As DataSet, ByVal procedure As String, ByVal table As String, ByVal hsh As Hashtable)
            Throw New NotImplementedException
        End Sub
    End Class
End Namespace