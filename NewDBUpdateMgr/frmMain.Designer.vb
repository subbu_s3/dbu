﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.ToTime = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.dtp_frm = New System.Windows.Forms.DateTimePicker()
        Me.txtbrowse = New System.Windows.Forms.TextBox()
        Me.Error_Success_Msg = New System.Windows.Forms.Label()
        Me.btnRevert = New System.Windows.Forms.Button()
        Me.btn_Save = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cmb_tblName = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dgvDatabase = New System.Windows.Forms.DataGridView()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ProgressBar1 = New System.Windows.Forms.ToolStripProgressBar()
        Me.STATUSLABEL = New System.Windows.Forms.ToolStripStatusLabel()
        Me.StatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.frmTime = New System.Windows.Forms.DateTimePicker()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvDatabase, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.frmTime)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.ToTime)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.dtp_frm)
        Me.GroupBox1.Controls.Add(Me.txtbrowse)
        Me.GroupBox1.Controls.Add(Me.Error_Success_Msg)
        Me.GroupBox1.Controls.Add(Me.btnRevert)
        Me.GroupBox1.Controls.Add(Me.btn_Save)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.cmb_tblName)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupBox1.Font = New System.Drawing.Font("Segoe UI Symbol", 9.0!)
        Me.GroupBox1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.GroupBox1.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(10)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1030, 69)
        Me.GroupBox1.TabIndex = 3
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Settings"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Segoe UI Symbol", 8.25!)
        Me.Label4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label4.Location = New System.Drawing.Point(803, 32)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(45, 13)
        Me.Label4.TabIndex = 11
        Me.Label4.Text = "To Time"
        '
        'ToTime
        '
        Me.ToTime.Font = New System.Drawing.Font("Segoe UI Symbol", 8.25!)
        Me.ToTime.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.ToTime.Location = New System.Drawing.Point(854, 28)
        Me.ToTime.Name = "ToTime"
        Me.ToTime.Size = New System.Drawing.Size(79, 22)
        Me.ToTime.TabIndex = 10
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Segoe UI Symbol", 8.25!)
        Me.Label3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label3.Location = New System.Drawing.Point(508, 32)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(31, 13)
        Me.Label3.TabIndex = 9
        Me.Label3.Text = "Date"
        '
        'dtp_frm
        '
        Me.dtp_frm.Font = New System.Drawing.Font("Segoe UI Symbol", 8.25!)
        Me.dtp_frm.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtp_frm.Location = New System.Drawing.Point(545, 29)
        Me.dtp_frm.Name = "dtp_frm"
        Me.dtp_frm.Size = New System.Drawing.Size(81, 22)
        Me.dtp_frm.TabIndex = 8
        '
        'txtbrowse
        '
        Me.txtbrowse.Font = New System.Drawing.Font("Segoe UI Symbol", 8.25!)
        Me.txtbrowse.Location = New System.Drawing.Point(73, 28)
        Me.txtbrowse.Name = "txtbrowse"
        Me.txtbrowse.Size = New System.Drawing.Size(264, 22)
        Me.txtbrowse.TabIndex = 7
        Me.txtbrowse.Text = "Browse Database"
        '
        'Error_Success_Msg
        '
        Me.Error_Success_Msg.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Error_Success_Msg.AutoSize = True
        Me.Error_Success_Msg.Font = New System.Drawing.Font("Segoe UI Symbol", 8.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Error_Success_Msg.ForeColor = System.Drawing.Color.Green
        Me.Error_Success_Msg.Location = New System.Drawing.Point(827, 11)
        Me.Error_Success_Msg.Name = "Error_Success_Msg"
        Me.Error_Success_Msg.Size = New System.Drawing.Size(0, 13)
        Me.Error_Success_Msg.TabIndex = 6
        '
        'btnRevert
        '
        Me.btnRevert.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnRevert.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnRevert.BackgroundImage = Global.NewDBUpdateMgr.My.Resources.Resources.Arrows_Undo_icon
        Me.btnRevert.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btnRevert.FlatAppearance.BorderSize = 0
        Me.btnRevert.Font = New System.Drawing.Font("Segoe UI Symbol", 8.25!)
        Me.btnRevert.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.btnRevert.Location = New System.Drawing.Point(983, 8)
        Me.btnRevert.Name = "btnRevert"
        Me.btnRevert.Size = New System.Drawing.Size(24, 24)
        Me.btnRevert.TabIndex = 5
        Me.btnRevert.UseVisualStyleBackColor = False
        Me.btnRevert.Visible = False
        '
        'btn_Save
        '
        Me.btn_Save.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_Save.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btn_Save.BackgroundImage = Global.NewDBUpdateMgr.My.Resources.Resources.Actions_document_save_icon
        Me.btn_Save.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btn_Save.FlatAppearance.BorderSize = 0
        Me.btn_Save.Font = New System.Drawing.Font("Segoe UI Symbol", 8.25!)
        Me.btn_Save.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.btn_Save.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_Save.Location = New System.Drawing.Point(1006, 8)
        Me.btn_Save.Name = "btn_Save"
        Me.btn_Save.Size = New System.Drawing.Size(24, 24)
        Me.btn_Save.TabIndex = 4
        Me.btn_Save.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_Save.UseVisualStyleBackColor = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Segoe UI Symbol", 8.25!)
        Me.Label2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label2.Location = New System.Drawing.Point(343, 32)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(42, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Tables "
        '
        'cmb_tblName
        '
        Me.cmb_tblName.Font = New System.Drawing.Font("Segoe UI Symbol", 8.25!)
        Me.cmb_tblName.FormattingEnabled = True
        Me.cmb_tblName.Location = New System.Drawing.Point(391, 29)
        Me.cmb_tblName.Name = "cmb_tblName"
        Me.cmb_tblName.Size = New System.Drawing.Size(96, 21)
        Me.cmb_tblName.TabIndex = 3
        Me.cmb_tblName.Text = "Select Table"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI Symbol", 8.25!)
        Me.Label1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label1.Location = New System.Drawing.Point(12, 32)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(55, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Database"
        '
        'dgvDatabase
        '
        Me.dgvDatabase.AllowUserToAddRows = False
        Me.dgvDatabase.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvDatabase.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDatabase.Location = New System.Drawing.Point(15, 72)
        Me.dgvDatabase.Name = "dgvDatabase"
        Me.dgvDatabase.Size = New System.Drawing.Size(1003, 260)
        Me.dgvDatabase.TabIndex = 4
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ProgressBar1, Me.STATUSLABEL, Me.StatusLabel1})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 332)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.StatusStrip1.Size = New System.Drawing.Size(1030, 22)
        Me.StatusStrip1.TabIndex = 5
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(100, 16)
        '
        'STATUSLABEL
        '
        Me.STATUSLABEL.Name = "STATUSLABEL"
        Me.STATUSLABEL.Size = New System.Drawing.Size(0, 17)
        '
        'StatusLabel1
        '
        Me.StatusLabel1.Name = "StatusLabel1"
        Me.StatusLabel1.Size = New System.Drawing.Size(0, 17)
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Segoe UI Symbol", 8.25!)
        Me.Label5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label5.Location = New System.Drawing.Point(653, 32)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(59, 13)
        Me.Label5.TabIndex = 13
        Me.Label5.Text = "From Time"
        '
        'frmTime
        '
        Me.frmTime.Font = New System.Drawing.Font("Segoe UI Symbol", 8.25!)
        Me.frmTime.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.frmTime.Location = New System.Drawing.Point(718, 28)
        Me.frmTime.Name = "frmTime"
        Me.frmTime.Size = New System.Drawing.Size(79, 22)
        Me.frmTime.TabIndex = 12
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1030, 354)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.dgvDatabase)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Database Updation"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.dgvDatabase, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents ToTime As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dtp_frm As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtbrowse As System.Windows.Forms.TextBox
    Friend WithEvents Error_Success_Msg As System.Windows.Forms.Label
    Friend WithEvents btnRevert As System.Windows.Forms.Button
    Friend WithEvents btn_Save As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cmb_tblName As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dgvDatabase As System.Windows.Forms.DataGridView
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ToolStripProgressBar
    Friend WithEvents STATUSLABEL As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents StatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents frmTime As System.Windows.Forms.DateTimePicker

End Class
