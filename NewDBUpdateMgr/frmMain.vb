﻿Imports System.Drawing
Imports System.IO
Imports System.Data
Imports NewDBUpdateMgr.NSDbUtility
Imports System.Data.OleDb

Public Class frmMain

    Enum MSGCOlor
        Red = 0
        Green = 1
    End Enum
    Dim ds As New DataSet()
    Dim db As New DbUtility
    Dim query As String

    Private Sub txtbrowse_Enter(sender As Object, e As EventArgs) Handles txtbrowse.Enter

        OpenFileDialog1.Title = "Open File..."
        OpenFileDialog1.Multiselect = False
        OpenFileDialog1.Filter = "Access 2000-2003 (*.mdb)|*.mdb|Access 2007 (*.accdb)|*accdb"
        OpenFileDialog1.ShowDialog()
        txtbrowse.Text = OpenFileDialog1.FileName
        GlobalVariable.SetGlobalInt(txtbrowse.Text)
        populateTableExists()
        'Dim ds As New DataSet()
        'db.executeQuery("GRANT SELECT ON MSysObjects TO Admin;")
        'ds = db.getDataSet("SELECT MSysObjects.Name AS table_name  FROM MSysObjects WHERE (((Left([Name],1))<>'~') AND ((Left([Name],4))<>'MSys') AND ((MSysObjects.Type) In (1,4,6))) order by MSysObjects.Name ")
        'If (ds Is Nothing) Then
        '    If (ds.Tables(0).Rows.Count > 0) Then
        '        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
        '            cmb_tblName.Items.Add(ds.Tables(0).Rows(i).Item(0))
        '        Next

        '    End If
        'End If
        StatusLabel1.Text = ""
        ProgressBar1.Value = 0
    End Sub
    Public Sub populateTableExists()
        Try

        
        Dim connstr As String = db.RETRIEVECONNECTION()
        Using conn As New OleDbConnection(connstr)
            conn.Open()
            Dim dt As DataTable = conn.GetSchema("Tables")
            Dim tables As String() = dt.AsEnumerable().Where((Function(dr) dr.Field(Of String)("TABLE_TYPE") = "TABLE")).Select(Function(dr) dr.Field(Of String)("TABLE_NAME")).ToArray
            For i As Integer = 0 To tables.Length - 1
                cmb_tblName.Items.Add(tables(i))
            Next
            End Using
        Catch ex As Exception

        End Try
    End Sub
    Function GetCheckedRows(grid As DataGridView, checkboxColumnIndex As Integer) As IEnumerable(Of DataGridViewRow)
        Dim checkedRows As New List(Of DataGridViewRow)
        For rowIndex As Integer = 0 To grid.RowCount - 1
            Dim chkCell = TryCast(grid(checkboxColumnIndex, rowIndex), DataGridViewCheckBoxCell)
            Dim isChecked As Boolean = CBool(chkCell.EditedFormattedValue)
            If isChecked Then
                checkedRows.Add(grid.Rows(rowIndex))
            End If
        Next
        Return checkedRows
    End Function
    Private Sub btn_Save_Click(sender As Object, e As EventArgs) Handles btn_Save.Click
        Try

            If (txtbrowse.Text Is Nothing Or txtbrowse.Text = "Browse Database") Then
                Msgboxshow("Browse Database......", 0)
            ElseIf (cmb_tblName.Text Is Nothing Or cmb_tblName.SelectedIndex = -1) Then
                Msgboxshow("Select Table Name.", 0)
            ElseIf DateTime.Compare(frmTime.Value, ToTime.Value) < 0 Then
                '(dtp_frm.Value.ToString("dd-mm-yyyy") > dtp_to.Value.ToString("dd-mm-yyyy")) Then

                Msgboxshow("Please enter valid date range.", 0)
            Else
                ProgressBar1.Value = 10
                Msgboxshow("", 0)
                Dim dgrows As IEnumerable(Of DataGridViewRow) = GetCheckedRows(dgvDatabase, dgvDatabase.ColumnCount - 1)
                For i As Integer = 0 To dgrows.Count - 1
                    query = "update [" & cmb_tblName.Text & "] set "

                    For j As Integer = 0 To dgvDatabase.ColumnCount - 1
                        Dim colType As Type = dgvDatabase.Rows(i).Cells(j).ValueType
                        If Not (dgvDatabase.Columns(j).HeaderText.ToUpper = "ID" Or dgvDatabase.Columns(j).HeaderText.ToUpper = "UPDATE") Then


                            If (j < dgvDatabase.ColumnCount - 2) Then

                                If (colType = GetType(DateTime)) Then
                                    query += "[" & dgvDatabase.Columns(j).HeaderText & "]=#" & dgrows(i).Cells(j).Value.ToString() & "#,"
                                ElseIf (colType = GetType(Integer) Or (colType = GetType(Byte)) Or (colType = GetType(Single))) Then
                                    query += "[" & dgvDatabase.Columns(j).HeaderText & "]=" & dgrows(i).Cells(j).Value.ToString() & ","


                                Else
                                    query += "[" & dgvDatabase.Columns(j).HeaderText & "]='" & dgrows(i).Cells(j).Value.ToString() & "',"


                                End If



                            Else

                                If (colType = GetType(DateTime)) Then
                                    query += "[" & dgvDatabase.Columns(j).HeaderText & "]=#" & dgrows(i).Cells(j).Value.ToString() & "#"
                                ElseIf (colType = GetType(Integer) Or (colType = GetType(Byte))) Then
                                    query += "[" & dgvDatabase.Columns(j).HeaderText & "]=" & dgrows(i).Cells(j).Value.ToString() & ""


                                Else
                                    query += "[" & dgvDatabase.Columns(j).HeaderText & "]='" & dgrows(i).Cells(j).Value.ToString() & "'"

                                End If

                            End If
                        End If

                        If (j = dgvDatabase.ColumnCount - 1) Then
                            query += " where [id]=" & dgrows(i).Cells("id").Value.ToString() & ""
                        End If
                    Next
                    ProgressBar1.Value = 10 * i
                    db.executeQuery(query)
                Next

                StatusLabel1.Text = ".......Data updated successfully"
                ProgressBar1.Value = 100
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Function Msgboxshow(ByVal txtmsg As String, ByVal showerrsucc As Integer) As String
        Error_Success_Msg.Text = txtmsg
        If (showerrsucc = 0) Then
            Error_Success_Msg.ForeColor = Color.Red
        Else
            Error_Success_Msg.ForeColor = Color.Green
        End If
        Return ""
    End Function

    Private Sub txtbrowse_TextChanged(sender As Object, e As EventArgs) Handles txtbrowse.TextChanged
        Try


            'Dim ds As New DataSet()
            'ds = db.getDataSet("SELECT MSysObjects.Name AS table_name  FROM MSysObjects WHERE (((Left([Name],1))<>'~') AND ((Left([Name],4))<>'MSys') AND ((MSysObjects.Type) In (1,4,6))) order by MSysObjects.Name ")
            'If (ds Is Nothing) Then
            '    If (ds.Tables(0).Rows.Count > 0) Then
            '        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
            '            cmb_tblName.Items.Add(ds.Tables(0).Rows(i).Item(0))
            '        Next

            '    End If
            'End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Label4_Click(sender As Object, e As EventArgs) Handles Label4.Click

    End Sub

    Private Sub cmb_tblName_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmb_tblName.SelectedIndexChanged
        'query = "select * from " & cmb_tblName.Text & " where Timestamp between #" & dtp_frm.Text & "#  and  #" & dtp_to.Text & "# order by Timestamp desc ".
        Try
            populatetable()
            StatusLabel1.Text = ""
            ProgressBar1.Value = 0
        Catch ex As Exception

        End Try


    End Sub


    'Private Sub btnRevert_Click(sender As Object, e As EventArgs)
    '    populatetable()
    '    StatusLabel1.Text = ""
    '    ProgressBar1.Value = 0
    'End Sub

    Public Sub populatetable()
        dgvDatabase.Columns.Clear()
        query = "select * from " & cmb_tblName.Text & ""
        ds = db.getDataSet(query)
        If Not (ds Is Nothing) Then
            If (ds.Tables(0).Rows.Count > 0) Then
                dgvDatabase.DataSource = ds.Tables(0)
                Dim chk As New DataGridViewCheckBoxColumn()
                dgvDatabase.Columns.Add(chk)
                chk.HeaderText = "Update"
                chk.Name = "chk"
            End If
          
        End If
    End Sub

   
    Private Sub dgvDatabase_CellValueChanged(sender As Object, e As DataGridViewCellEventArgs) Handles dgvDatabase.CellValueChanged
        If (dgvDatabase.Columns(e.ColumnIndex).CellType.Name = "DataGridViewCheckBoxCell") Then
        Else
            Dim chk As DataGridViewCheckBoxCell = DirectCast(dgvDatabase.Rows(e.RowIndex).Cells(dgvDatabase.Columns.Count - 1), DataGridViewCheckBoxCell)
            chk.Value = True

        End If



    End Sub

    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
End Class
